import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

import digraphs.Digraph;
import digraphs.DigraphGenerator;
import model.Arc;
import model.ArcType;
import model.PNMLPIPEformat;
import model.Place;
import model.ProcessPN;
import model.S4PR;
import model.Transition;

public class createS4PR {
	private String outputfilename;
	
	int totalPlaces = 0;
	int totalTransitions = 0;
	
	// EXPERIMENT PARAMETERS -- IEEE SMC Systems
	private int MIN_RESOURCES;
	private int MAX_RESOURCES; // MIN_RESOURCES*3/2
	private int MIN_INITIALMARKING = 2;
	private int MAX_INITIALMARKING = 5;
	private int MIN_RESINITIALMARKING = 2;
	private int MAX_RESINITIALMARKING = 3;
	private int MIN_TRANSITIONRATE = 1;
	private int MAX_TRANSITIONRATE = 2;
	private int MIN_PPNS_PER_RESOURCE = 2;
	private int	MAX_PPNS_PER_RESOURCE = 3;
	
	// Random parameters
	private long seed = System.currentTimeMillis();
	private Random random = new Random(seed);
	
	ArrayList<ArrayList<Integer>> resourcePPNs = new ArrayList<ArrayList<Integer>>(1);
	

	public static void printUsage()
	{
		System.out.println("Insufficient parameters.\n"
				+ "Usage: createS4PR NPPN NRES MINPLACES MAXPLACES [-im min max]"
				+ 						" [-rm min max] [-tr min max] [-pr min max]"
				+ "\n\tNPPN\t\t\tNumber of process PNs"
				+ "\n\tNRES\t\t\tNumber of resources"
				+ "\n\tMINPLACES\t\tMinimum number of places per PPN"
				+ "\n\tMAXPLACES\t\tMaximum number of places per PPN"
				+ "\n\tMAXPLACES\t\tMaximum number of places per PPN"
				+ "\n\t-im min max\t\tMinimum and maximum number of initial tokens (in PPN) [2, 5 by default]"
				+ "\n\t-rm min max\t\tMinimum and maximum number of initial tokens (in resources) [2, 3 by default]"
				+ "\n\t-tr min max\t\tMinimum and maximum transition rates [1, 2 by default]"
				+ "\n\t-pr min max\t\tMinimum and maximum process PNs in which a resource is used [2, 3 by default]"
				+ "\n\nExample: createS4PR 5 3 8 12");
		System.exit(-1);
	}
	
	public void parseParameters(String[] args, int nPPNs)
	{
		try{
			for(int i = 4; i < args.length;)
			{
				if(args[i].equals("-im"))
				{
					MIN_INITIALMARKING = Integer.parseInt(args[i + 1]);
					MAX_INITIALMARKING = Integer.parseInt(args[i + 2]);
				}else if(args[i].equals("-rm"))
				{
					MIN_RESINITIALMARKING = Integer.parseInt(args[i + 1]);
					MAX_RESINITIALMARKING = Integer.parseInt(args[i + 2]);
				}else if(args[i].equals("-tr"))
				{
					MIN_TRANSITIONRATE = Integer.parseInt(args[i + 1]);
					MAX_TRANSITIONRATE = Integer.parseInt(args[i + 2]);
				}else if(args[i].equals("-pr"))
				{
					MIN_PPNS_PER_RESOURCE = Integer.parseInt(args[i + 1]);
					MAX_PPNS_PER_RESOURCE = Integer.parseInt(args[i + 2]);
				}
				i += 3;
			}
		}
		catch(Exception e)
		{
			createS4PR.printUsage();
		}
		
		assert(MIN_INITIALMARKING <= MAX_INITIALMARKING);
		assert(MIN_RESINITIALMARKING <= MAX_RESINITIALMARKING);
		assert(MIN_TRANSITIONRATE <= MAX_TRANSITIONRATE);
		assert(MIN_PPNS_PER_RESOURCE <= MAX_PPNS_PER_RESOURCE &&  nPPNs <= MAX_PPNS_PER_RESOURCE);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/* It receives as parameter the source and the destiny file */
		
		int 	nPPNs,// = 10, // 5
				minPlaces,// = //8, 20, 
				maxPlaces,// = //12, 25,
				nResources;// = 10; // 5
		
		if(args.length >= 4)
		{
			try{
				nPPNs = Integer.parseInt(args[0]);
				nResources = Integer.parseInt(args[1]);
				minPlaces = Integer.parseInt(args[2]);
				maxPlaces = Integer.parseInt(args[3]);
			
				// Check input values
				assert(minPlaces <= maxPlaces);
				
				createS4PR createS4PR = new createS4PR();
				
				createS4PR.parseParameters(args, nPPNs);
				
				createS4PR.createRandomS4PR(nPPNs, nResources, minPlaces, maxPlaces, "s4pr_" +   
												new SimpleDateFormat("yyMMdd_hhmmss").format(Calendar.getInstance().getTime()));
			}
			catch(Exception e)
			{
				printUsage();
			}
		}
		else
			printUsage();
	}
	
	private int randInt(int min, int max) {
	    int randomNum = random.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	private void consumeResource(int idxPPN,
									ProcessPN auxNet, Place pr)
	{
		// Get a random place from available, and remove it
		int aux = randInt(0, auxNet.getValidPlaces().size() - 1);
		Place randomPlace = auxNet.getValidPlaces().get(aux);
		auxNet.removeValidPlace(aux);
		
		// Create two new places, acquire, and "use" transitions
		Place acqPlace = createPlace(false, false, idxPPN + "acq" + "_" + auxNet.getPlaces().size());
		auxNet.addPlace(acqPlace);
		Place auxPlace = createPlace(false, false, idxPPN + "aux" + "_"  + auxNet.getPlaces().size());
		auxNet.addPlace(auxPlace);

		updateInputArcs(auxNet.getArcs(), randomPlace, acqPlace);
		
		// Connect them
		Transition acqTrans = connectPlaces(idxPPN, acqPlace, auxPlace, true, auxNet.getTransitions(), auxNet.getArcs());
		// Connect them
		Transition auxTrans = connectPlaces(idxPPN, auxPlace, randomPlace, false, auxNet.getTransitions(), auxNet.getArcs());
		
		// Connect to resource r
		createArc(pr, acqTrans, auxNet.getArcs());
		createArc(auxTrans, pr, auxNet.getArcs());

		System.out.println("Connecting " + pr.getLabel() + " to PPN " + idxPPN + "(thru. " + randomPlace.getLabel() + ")");
	}

	private void updateInputArcs(ArrayList<Arc> arcs, 
									Place randomPlace, Place acqPlace)
	{
		int idx = randomPlace.get_id();
		for(int i = 0; i < arcs.size(); i++)
		{
			Arc arc = arcs.get(i);
			if(arc.getDstLabel() == 'P' && arc.getDst() == idx)
			{
				arc.setDst(acqPlace.get_id());
				arc.setDstStr(acqPlace.getLabel());
			}
		}
		
	}

	private S4PR createS4PRfromDAGs(int nPPNs, int maxPlaces, int maxTransitions)
	{
		ArrayList<ProcessPN> ppns = new ArrayList<ProcessPN>(1);
		ArrayList<Place> resources = new ArrayList<Place>(1);

		// First, create the process PNs
		ArrayList<ProcessPN> eligiblePPN = new ArrayList<ProcessPN>(1);
		
		for(int i = 0; i < nPPNs; i++)
		{
			ProcessPN ppn = createPPNfromRandomDAG(i, maxPlaces, maxTransitions);
			ppns.add(ppn);
			eligiblePPN.add(ppn);
		}
		
		int auxResources = randInt(this.MIN_RESOURCES, this.MAX_RESOURCES);
		
		// Then, add resources
		for(int i = 0; i < auxResources; i++)
		{
			Place pr = createPlace(true, true, "r_" + i);
			resources.add(pr);
			
			ArrayList<Integer> ppnsPerRes = new ArrayList<Integer>(1);
			// Add to resource -> PPN
			resourcePPNs.add(ppnsPerRes);
			
			int maxPPNs = randInt(MIN_PPNS_PER_RESOURCE, MAX_PPNS_PER_RESOURCE);
			for(int j = 0; j < maxPPNs; j++)
			{
				// Select a PPN
				ProcessPN selectedPPN;
				if(eligiblePPN.size() > 0)
				{
					selectedPPN = eligiblePPN.get(randInt(0, eligiblePPN.size() - 1));
					eligiblePPN.remove(selectedPPN);
				}else
				{
					ArrayList<Set<Integer>> sccs = computeSCCs(resourcePPNs, ppns.size());
					if(sccs.size() > 1)
					{
						// Build again set of eligible nets
						for(int jj = 0; jj < sccs.size(); jj++)
						{
							Set<Integer> auxV = sccs.get(jj);
							eligiblePPN.add(ppns.get((Integer) auxV.toArray()[randInt(0, auxV.size() - 1)]));
						}
						selectedPPN = eligiblePPN.get(randInt(0, eligiblePPN.size() - 1));
						eligiblePPN.remove(selectedPPN);
					}else
						selectedPPN = ppns.get(randInt(0, ppns.size() - 1));
				}
				// Acquire/release resource in selection
				consumeResource(ppns.indexOf(selectedPPN), selectedPPN, pr);
				ppnsPerRes.add(ppns.indexOf(selectedPPN));
			}
		}
		
		// Create sets		
		ArrayList<Place> places = new ArrayList<Place>(1);
		ArrayList<Transition> transitions = new ArrayList<Transition>(1);
		ArrayList<Arc> arcs = new ArrayList<Arc>(1);
		
		for(ProcessPN ppn : ppns)
		{
			places.addAll(ppn.getPlaces());
			transitions.addAll(ppn.getTransitions());
			arcs.addAll(ppn.getArcs());
		}
		places.addAll(resources);
		
		S4PR s4pr = new S4PR(ppns, resources, places, transitions, arcs);
		
		return s4pr;
	}
	
	/*public void createRandomS4PR(String filename)
	{		
		S4PR s4pr = createS4PRfromDAGs(nPPNs, nPlaces, nTransitions, nResources);
		
		try {
			saveToPNML(filename, s4pr.getAllPlaces(), s4pr.getAllTransitions(), s4pr.getAllArcs());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}*/
	
	// filename comes *without extension*
	public void createRandomS4PR(int nPPNs, int nResources, int minPlaces, int maxPlaces,  String filename)
	{
		int numPlaces = randInt(minPlaces, maxPlaces);
		int numTrans = numPlaces*7/6;
		
		this.MIN_RESOURCES = nResources;
		this.MAX_RESOURCES = MIN_RESOURCES*3/2;
		
		S4PR s4pr = createS4PRfromDAGs(nPPNs, numPlaces, numTrans);
		
		// Create a name with some meaning for the XML file
		outputfilename = filename 	+ "_" + nPPNs + 	"PPN_" +
									s4pr.getAllPlaces().size() + "p_" + 
									s4pr.getAllTransitions().size() + "t_" + 
									s4pr.getResources().size() + "r.xml";
		System.out.println("Saving to [" + outputfilename + "]");
		try {
			saveToPNML(outputfilename, s4pr.getAllPlaces(), s4pr.getAllTransitions(), s4pr.getAllArcs());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/*public void createRandomDAGNet(String filename)
	{
	
		ProcessPN ppn = createPPNfromRandomDAG(0, 8, 8);
		try {
			saveToPNML(filename, ppn.getPlaces(), ppn.getTransitions(), ppn.getArcs());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}*/
	
	private ProcessPN createPPNfromRandomDAG(int idxPPN, int maxPlaces, int maxTransitions)
	{
		// Create a new net
		Digraph g = DigraphGenerator.dag(maxPlaces, maxTransitions);
		ProcessPN ppn = new ProcessPN();		
		convertDAG2PPN(g, ppn, idxPPN);
		return ppn;
	}
	
	private void convertDAG2PPN(Digraph g, ProcessPN ppn, int idxPPN)
	{
		int initPlaces = totalPlaces;
		//int initTrans = totalTransitions;
		
		// Create empty sets
		ArrayList<Place> places = new ArrayList<Place>(1);
		ArrayList<Transition> transitions = new ArrayList<Transition>(1);
		ArrayList<Arc> arcs = new ArrayList<Arc>(1);
		
		// Each node in g is a Place, and edges are transitions
		// Get list of vertex
		int nPlaces = g.V();
		//int nTrans = g.E();
		
		boolean[] hasSomeInput = new boolean[nPlaces]; 
		boolean[] hasSomeOutput = new boolean[nPlaces];
		int[] numAdj = new int[nPlaces];
		
		for(int i = 0; i < nPlaces; i++)
		{
			Iterable<Integer> it = g.adj(i);
			for(Integer j : it)
			{
				hasSomeOutput[i] = true;
				hasSomeInput[j.intValue()] = true;
				numAdj[i]++;
			}
		}
		
		// Check places without connection
		ArrayList<Integer> vertexToAvoid = new ArrayList<Integer>(1);
		Place[] auxIds = new Place[nPlaces];
		
		for(int i = 0; i < nPlaces; i++)
		{
			if(!hasSomeOutput[i] && !hasSomeInput[i])
			{
				System.out.println("Place P" + i + " w/o inputs nor outputs!");
				vertexToAvoid.add(i);
			}else
			{
				Place p = createPlace(false, false, idxPPN + "_" + places.size());
				places.add(p);
				auxIds[i] = p;
			}
		}
		

		hasSomeInput = new boolean[places.size()]; 
		hasSomeOutput = new boolean[places.size()];
		
		for(int i = 0; i < nPlaces; i++)
		{
			if(vertexToAvoid.contains(i))
				continue;
			
			Place p = auxIds[i];
			Iterable<Integer> it = g.adj(i);
			
			// Create transitions
			for(Integer j : it)
			{
				// Create connection arcs between them
				//connectPlaces(idxPPN, p, auxIds[j], (numAdj[i] > 1), transitions, arcs);
				connectPlaces(idxPPN, p, auxIds[j], false, transitions, arcs);
				hasSomeInput[auxIds[j].get_id() -  initPlaces] = true;
				hasSomeOutput[p.get_id() - initPlaces] = true;
			}
		}
		
		// Now, make the net cyclic (close it)
		Place	p0 	= createPlace(true, false, idxPPN + "i_" + places.size());
		Place pEnd 	= createPlace(false, false, idxPPN + "e_" + (places.size() + 1));
		
		ppn.addValidPlace(places);
		places.add(p0);
		places.add(pEnd);
		connectPlaces(idxPPN, p0, pEnd, false, transitions, arcs);	
		
		// Create connection arcs between them
		for(int i = 0; i < (places.size() - 2); i++)
		{
			if(!hasSomeInput[i])
			{
				System.out.println("Adding a connection between Pend and P" + i + "...");
				connectPlaces(idxPPN, pEnd, places.get(i), true, transitions, arcs);
			}
			if(!hasSomeOutput[i])
			{
				System.out.println("Adding a connection between P" + i + " and P0...");
				connectPlaces(idxPPN, places.get(i), p0, false, transitions, arcs);
			}
		}
		
		System.out.println("Process PN created: " + places.size() + " places, " + transitions.size() + " transitions");
		for(Place p : places)
			ppn.addPlace(p);
		for(Transition t : transitions)
			ppn.addTransition(t);
		for(Arc a : arcs)
			ppn.addArc(a);
		ppn.setP0Place(p0);
	}

	private Transition connectPlaces(int idxPPN, 
								Place pSrc, Place pDst, boolean joinWithImmediate,
								ArrayList<Transition> transitions,
								ArrayList<Arc> arcs) {
		Transition t = createTransition(idxPPN + "_" + transitions.size());
		if(joinWithImmediate)
			changeTimedToImmediateTransition(t);
		
		//System.out.println("Creating arc from " + pSrc + " to " + pDst);
		
		transitions.add(t);
		createArc(pSrc, t, arcs);
		createArc(t, pDst, arcs);
		
		return t;
	}

	private Arc createArc(Place pSrc, Transition t, ArrayList<Arc> arcs)
	{
		// Create pre arc
		Arc a = new Arc();
		a.setWeight(1);
		a.setSrc(pSrc.get_id());
		a.setSrcLabel('P');
		a.setDst(t.get_id());
		a.setDstLabel('T');
		a.setType(ArcType.NORMAL);		
		a.setSrcStr(pSrc.getLabel());
		a.setDstStr(t.getLabel());
		arcs.add(a); // Add arc
		
		
		return a;
	}
	
	private Arc createArc(Transition t, Place pDst, ArrayList<Arc> arcs)
	{
		// Create pre arc
		// Create post arc
		Arc a = new Arc();
		a.setWeight(1);
		a.setSrc(t.get_id());
		a.setSrcLabel('T');
		a.setDst(pDst.get_id());
		a.setDstLabel('P');
		a.setType(ArcType.NORMAL);
		a.setDstStr(pDst.getLabel());
		a.setSrcStr(t.getLabel());
		arcs.add(a); // Add arc
		
		return a;
	}

	/*private void serializeToFile(S4PR s4pr, String filename) throws IOException {
		FileOutputStream fos = new FileOutputStream(filename); 
		ObjectOutputStream out = new ObjectOutputStream(fos);
		
		out.writeObject(s4pr);
		
		out.flush();
		out.close();
		fos.close();
	}*/
	
	private Place createPlace(boolean setInitialMarking, boolean isResourcePlace, String sidx)
	{
		int		auxMin = 0, 
				auxMax = 0;
		if(!isResourcePlace)
		{
			auxMin = MIN_INITIALMARKING;
			auxMax = MAX_INITIALMARKING;
		}else{
			auxMin = MIN_RESINITIALMARKING;
			auxMax = MAX_RESINITIALMARKING;
		}
		Place p = new Place();
		
		p.set_id(totalPlaces++);
		p.setLabel("P" + sidx);
		p.setInitialMarking(setInitialMarking ? randInt(auxMin, auxMax):0); 
		p.setX(randInt(1, 640));
		p.setY(p.getX() + randInt(1, 480));
		return p;
	}
	
	private void changeTimedToImmediateTransition(Transition t)
	{
		t.setRate(1.0); // Set rate of immediate transition to 1
		t.setTimed(false);
		t.setLabel(t.getLabel().toLowerCase()); // Change to lower case when immediate
	}
	
	private Transition createTransition(String sidx)
	{
		Transition t = new Transition();
		
		t.set_id(totalTransitions++);
		t.setLabel("T" + sidx);
		t.setRate(randInt(MIN_TRANSITIONRATE, MAX_TRANSITIONRATE));
		t.setTimed(true);
		t.setInfiniteServer(true);
		t.setX(randInt(1, 640));
		t.setY(t.getX() + randInt(1, 480));
		
		return t;
	}
	
	/*public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[Filename: " + this.outputfilename + "; PPNs: " +
						this.nProcesses + "; Resources: " + 
						this.nResources + "; Max. transitions per PPN: " + 
						this.nTransitionsPerProcess +
						"]");
		
		return sb.toString();
	}*/
	
	private void saveToPNML(String filename,
								ArrayList<Place> places,
								ArrayList<Transition> transitions,
								ArrayList<Arc> arcs) 
										throws IOException {
		FileOutputStream fos = new FileOutputStream(filename); 
		OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8");
		
		PNMLPIPEformat pnml = new PNMLPIPEformat();
		
		out.write(pnml.createFileHeader());
		
		/* Iterate on places */
		for(Place p : places)
		{
			out.write(pnml.createPlaceNode(p));
		}
		
		/* Iterate on transitions */
		for(Transition t : transitions)
		{
			out.write(pnml.createTransitionNode(t));
		}
		
		/* Iterate on arcs */
		for(Arc a : arcs)
		{
			out.write(pnml.createArcNode(a));
		}
		
		out.write(pnml.createFileTail());
		/* Close everything */
		out.flush();
		out.close();
		fos.close();
	}
	
	
	private ArrayList<Set<Integer>> computeSCCs(ArrayList<ArrayList<Integer>> rList, int nPPNs)
	{
		int[][] matrix = new int[nPPNs][nPPNs];
		
		for(int i = 0; i < rList.size(); i++)
		{
			ArrayList<Integer> sublistPPNs = rList.get(i);
			
			for(int j = 0; j < sublistPPNs.size(); j++)
				for(int k = 0; k < sublistPPNs.size(); k++)
					if(j != k)
						matrix[sublistPPNs.get(j)][sublistPPNs.get(k)] = 1;
		}
		
		return computeSCCsTarjanAlg(matrix);
	}
	
	private ArrayList<Set<Integer>> computeSCCsTarjanAlg(int[][] adjacentMatrix)
	{
		ArrayList<Set<Integer>> sccsList = new ArrayList<Set<Integer>>(1); 
		int[] index = new int[adjacentMatrix.length];
		for(int i = 0; i < index.length; i++)
			index[i] = -1;
		
		int[] lowlink = new int[adjacentMatrix.length];
		boolean[] onStack = new boolean[lowlink.length];
		
		Stack<Integer> stack = new Stack<Integer>();
		
		int idx = 0;
		for(int i = 0; i < index.length; i++)
			if(index[i] == -1)
				idx = strongconnect(adjacentMatrix, i, idx, index, lowlink, onStack, stack, sccsList);
		
		return sccsList;
	}

	private int strongconnect(int[][] adjacentMatrix, int v, int idx, int[] index, int[] lowlink, boolean[] onStack, 
								Stack<Integer> stack, ArrayList<Set<Integer>> sccsList)
	{
		index[v] = idx;
		lowlink[v] = idx;
		idx++;
		
		stack.push(v);
		onStack[v] = true;
		
		for(int i = 0; i < adjacentMatrix.length; i++)
			if(adjacentMatrix[v][i] != 0)
			{
				if(index[i] == -1)
				{
					idx += strongconnect(adjacentMatrix, i, idx, index, lowlink, onStack, stack, sccsList); 
					lowlink[v] = Math.min(lowlink[v], lowlink[i]);
				}
				else if(onStack[i])
				{
					lowlink[v] = Math.min(lowlink[v], lowlink[i]);
				}
			}
		
		if(lowlink[v] == index[v])
		{
			Set<Integer> scc = new HashSet<Integer>(1);
			sccsList.add(scc);
			int w;
			do
			{
				w = stack.pop();
				onStack[w] = false;
				scc.add(w);
			}while( w != v);
			
		}
		
		return idx;
	}
	
}
