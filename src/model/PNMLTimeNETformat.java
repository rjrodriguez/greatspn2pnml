/**
 * Filename: PNMLformat.java
 * Date: February, 2012 -- first release
 * Class for defining how PNML nodes are. 
 * TODO Adapt to work directly with PNML framework
 *   
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */

package model;

import java.util.Locale;

public class PNMLTimeNETformat extends PNMLformat{
	private int nPlaces = 0;
	private int nTransitions = 0;
	private int lastIdArc = 0;
	
	public String createFileHeader()
	{
		StringBuilder s = new StringBuilder();
		s.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		s.append("<net id=\"0\" netclass=\"eDSPN\"\n" +
					"\txmlns=\"http://pdv.cs.tu-berlin.de/TimeNET/schema/eDSPN\"\n" +
					"\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
					" xsi:schemaLocation=\"http://pdv.cs.tu-berlin.de/TimeNET/schema/eDSPN etc/schemas/eDSPN.xsd\">\n");
		
		return s.toString();
	}

	public String createFileTail()
	{
		return "</net>";
	}

	
	public String createPlaceNode(Place p)
	{
		StringBuilder s = new StringBuilder();
		
		// XXX We assume all follows the nomenclature "0."(place id) as its id
		/* Init place description */
		//s.append("\t<place id=\"P" + p.get_id() + "\">\n");
		s.append("\t<place id=\"0." + p.get_id() + "\" initialMarking=\"" + p.getInitialMarking() + "\" type=\"node\">\n");
		// Graphics orientation
		s.append("\t\t<graphics orientation=\"0\" x=\"" + (int)p.getX() + "\" y=\"" + (int)p.getY() + "\"/>\n");
		// Label
		s.append("\t\t<label id=\"0." + p.get_id() + ".0\" text=\"" + p.getLabel() + "\" type=\"text\">\n");
		s.append("\t\t\t<graphics x=\"-10\" y=\"-40\"/>\n");
		s.append("\t\t</label>\n");
		/* End place description */
		s.append("\t</place>\n");
		
		nPlaces++; // Needed since TimeNET uses just one id, places and transitions are both node elements!
		return s.toString();
	}

	public String createTransitionNode(Transition t)
	{
		StringBuilder s = new StringBuilder();
		// XXX We assume all follows the nomenclature "0."(transition id) as its id
		/* Init transition description */
		int auxTransID = t.get_id() + this.nPlaces;
		if(t.isTimed())
		{
			s.append("\t<exponentialTransition DTSPNpriority=\"1\" delay=\"" + 
								String.format(Locale.ENGLISH, "%f", 1/t.getRate())  + "\"" + 
						" id=\"0."+ auxTransID + "\"" +
						" preemptionPolicy=\"PRD\"" +
						" serverType=\"" + (t.isInfiniteServer() ? "InfiniteServer" : "ExclusiveServer") + "\"" +
						" type=\"node\">\n");
		}else
		{	// Immediate
			s.append("\t<immediateTransition enablingFunction=\"\" id=\"0."+ auxTransID + "\"" +
					 " priority=\"" + t.getPriority() + "\"" +
					 " type=\"node\" weight=\"" + String.format(Locale.ENGLISH, "%.4f", t.getWeight()) + "\">\n");
		}
		// Graphics orientation
		s.append("\t\t<graphics orientation=\"0\" x=\"" + (int)t.getX() + "\" y=\"" + (int)t.getY() + "\"/>\n");
		// Label
		s.append("\t\t<label id=\"0." + auxTransID + ".0\" text=\"" + t.getLabel() + "\" type=\"text\">\n");
		s.append("\t\t\t<graphics x=\"-10\" y=\"-40\"/>\n");
		s.append("\t\t</label>\n");
		
		/* End transition description */
		s.append(t.isTimed() ? "\t</exponentialTransition>\n" : "\t</immediateTransition>\n");
		
		nTransitions++;
		return s.toString();
	}
	
	public String createArcNode(Arc a)
	{
		StringBuilder s = new StringBuilder();
		if(lastIdArc == 0)
			lastIdArc = (nPlaces + nTransitions);
		else
			lastIdArc++;
		
		/* Init arc node */
		s.append( "\t<arc fromNode=\"0." + (a.getSrcLabel() == 'P' ? a.getSrc() : a.getSrc() + nPlaces) + "\"" +
					" id=\"0." + lastIdArc + "\"" +
					" toNode=\"0." + (a.getDstLabel() == 'P' ? a.getDst() : a.getDst() + nPlaces)  + "\"" +
					" type=\"connector\">\n" );
		// Inscription
		s.append("\t\t<inscription id=\"0." + lastIdArc + "\"" +
				 	" text=\"" + a.getWeight() + "\"" +
				 	" type=\"inscriptionText\">\n"); 
		s.append("\t\t\t<graphics x=\"0\" y=\"0\"/>\n");
		s.append("\t\t</inscription>\n");
		/* End arc node */
		s.append( "\t</arc>\n");

		
		return s.toString();
	}
}
