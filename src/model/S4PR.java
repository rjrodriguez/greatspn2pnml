package model;

import java.io.Serializable;
import java.util.ArrayList;

public class S4PR implements Serializable {
	private static final long serialVersionUID = 9011047551062672377L;
	private ArrayList<ProcessPN> nets;
	private ArrayList<Place> resources;
	
	private ArrayList<Place> allPlaces = new ArrayList<Place>(1);
	private ArrayList<Transition> allTransitions = new ArrayList<Transition>(1);
	private ArrayList<Arc> allArcs = new ArrayList<Arc>(1);
	
	public S4PR(ArrayList<ProcessPN> ppns, ArrayList<Place> resources,
					ArrayList<Place> places,
					ArrayList<Transition> transitions,
					ArrayList<Arc> arcs)
	{
		this.nets = ppns;
		this.resources = resources;
		this.allPlaces = places;
		this.allTransitions = transitions;
		this.allArcs = arcs;
	}
	
	/*private void addProcessPN(ArrayList<ProcessPN> ppns)
	{
		for(int i = 0; i < ppns.size(); i++)
			addProcessPN(ppns.get(i));
	}
	
	private void addProcessPN(ProcessPN net)
	{
		nets.add(net);
		allPlaces.addAll(net.getPlaces());
		allTransitions.addAll(net.getTransitions());
		allArcs.addAll(net.getArcs());
	}*/
	
	public ProcessPN getProcessPN(int i)
	{
		return nets.get(i);
	}
	
	public ArrayList<ProcessPN> getPPNs()
	{
		return nets;
	}
	
	public ArrayList<Place> getAllPlaces()
	{
		return allPlaces;
	}
	
	public ArrayList<Transition> getAllTransitions()
	{
		return allTransitions;
	}
	
	public ArrayList<Arc> getAllArcs()
	{
		return allArcs;
	}
	
	public int getLength()
	{
		return nets.size();
	}
	
	public ArrayList<Place> getResources()
	{
		return resources;
	}
}
