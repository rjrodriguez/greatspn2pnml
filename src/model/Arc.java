/**
 * Filename: Arc.java
 * Date: February, 2012 -- first release
 * Defines an arc at PN level
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */
package model;

public class Arc
{
	private int src, dst;
	private String srcStr, dstStr;
	private char srcLabel, dstLabel;
	private int weight;
	private ArcType type;
	
	/* Public getters & setters */
	public int getSrc() {
		return src;
	}
	public void setSrc(int src) {
		this.src = src;
	}
	public int getDst() {
		return dst;
	}
	public void setDst(int dst) {
		this.dst = dst;
	}
	public char getSrcLabel() {
		return srcLabel;
	}
	public void setSrcLabel(char srcLabel) {
		this.srcLabel = srcLabel;
	}
	public char getDstLabel() {
		return dstLabel;
	}
	public void setDstLabel(char dstLabel) {
		this.dstLabel = dstLabel;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public ArcType getType() {
		return type;
	}
	public void setType(ArcType type) {
		this.type = type;
	}
	public String getSrcStr() {
		return this.srcStr;
	}
	public String getDstStr() {
		return this.dstStr;
	}
	public void setSrcStr(String srcStr) {
		this.srcStr = srcStr;
	}
	public void setDstStr(String dstStr) {
		this.dstStr = dstStr;
	}
}