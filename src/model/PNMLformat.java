package model;

public abstract class PNMLformat {
	public abstract String createFileHeader();
	public abstract String createFileTail();	
	public abstract String createPlaceNode(Place p);
	public abstract String createTransitionNode(Transition t);	
	public abstract String createArcNode(Arc a);
}
