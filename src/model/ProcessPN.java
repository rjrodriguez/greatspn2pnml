package model;

import java.util.ArrayList;

public class ProcessPN {
	private ArrayList<Place> places;
	private ArrayList<Transition> transitions;
	private ArrayList<Arc> arcs;
	private Place p0;
	
	private ArrayList<Place> validPlaces;
	
	public ProcessPN()
	{
		places = new ArrayList<Place>(1);
		transitions = new ArrayList<Transition>(1);
		arcs = new ArrayList<Arc>(1);
		validPlaces = new ArrayList<Place>(1);
	}
	
	public ArrayList<Place> getValidPlaces()
	{
		return validPlaces;
	}
	
	public void addPlace(Place p)
	{
		places.add(p);
	}
	
	public void addTransition(Transition t)
	{
		transitions.add(t);
	}
	
	public void addArc(Arc a)
	{
		arcs.add(a);
	}
	
	public ArrayList<Place> getPlaces()
	{
		return places;
	}
	
	public ArrayList<Transition> getTransitions()
	{
		return transitions;
	}
	
	public ArrayList<Arc> getArcs()
	{
		return arcs;
	}
	
	public void setP0Place(Place p0)
	{
		this.p0 = p0;
	}
	
	public Place getP0Place()
	{
		return this.p0;
	}
	
	public void addValidPlace(Place p) {
		this.validPlaces.add(p);
	}
	
	public void removeValidPlace(int idxElement) {
		this.validPlaces.remove(idxElement);
	}
	
	public void addValidPlace(ArrayList<Place> places) {
		validPlaces = (ArrayList<Place>) places.clone();
	}
	
}
