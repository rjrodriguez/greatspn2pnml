/**
 * Filename: ArcType.java
 * Date: February, 2012 -- first release
 * Defines an arc type, either NORMAL arc or INHIBITOR arc
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */
package model;

public enum ArcType
{
	NORMAL, INHIBITOR;
}