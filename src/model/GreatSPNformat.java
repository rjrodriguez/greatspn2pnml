/**
 * Filename: GreatSPNformat.java
 * Date: February, 2012 -- first release
 * Contains the definition of place, transition and arc from a line readed from a GreatSPN format file (.net)
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */
package model;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class GreatSPNformat {

	private final int FACTOR = 100;
	
	public class TransitionArcs
	{
		public Transition t;
		public ArrayList<Arc> arcs;
		
		TransitionArcs(Transition t, ArrayList<Arc>arcs)
		{
			this.t = t;
			this.arcs = arcs;
		}
	}
	
	public class PlaceArc
	{
		public Place place;
		public int weight;
		
		public PlaceArc(Place place, int weight)
		{
			this.place = place;
			this.weight = weight;
		}
	}
	
	public class TransitionThr
	{
		public double thrValue;
		public String labelID;
		
		public TransitionThr(String labelID, double thrValue)
		{
			this.thrValue = thrValue;
			this.labelID = labelID;
		}
	}
	
	public TransitionArcs processNewTransition(ArrayList<Place> places, int id, int[] groups, String strLine, BufferedReader br, boolean changelabels) throws IOException {
		/* This is a transition, create a new node to him */
		Transition t = new Transition();
		ArrayList<Arc> arcs = new ArrayList<Arc>(1);
		
		strLine = strLine.replaceAll(" +", ";");
		String[] split = strLine.split(";");
		t.set_id(id);
		t.setLabel(changelabels ? "t" + id + "_" + split[0] : split[0]);
		t.setRate(Double.parseDouble(split[1]));
		
		t.setTimed(Integer.parseInt(split[3]) == 0);
		//XXX Either infinite or single server semantics, no other allowed
		t.setInfiniteServer((t.isTimed() && (Integer.parseInt(split[2]) == 0)) || !t.isTimed());
		// XXX Infinite server semantics also for immediate transitions
	
		// Set weight and priority for immediate transitions
		if(!t.isTimed())
		{
			t.setWeight(t.getRate()); // It was set as rate previously
			t.setPriority(groups[Integer.parseInt(split[3]) - 1]);
		}
		
		
		switch(Integer.parseInt(split[5]))
		{
			case 0: 
					t.setOrientation(90);
					break;
			case 1:
					t.setOrientation(0);
					break;
			case 2:
					t.setOrientation(135);
					break;
			case 3:
					t.setOrientation(45);
					break;
		}
		
		t.setX(Double.parseDouble(split[6])*FACTOR);
		t.setY(Double.parseDouble(split[7])*FACTOR);
		
		/* Iterate for each input arc*/
		int inputArcs = Integer.parseInt(split[4]);
		for(int i = 0; i < inputArcs; i++)
		{
			/* Read new arc */
			strLine = br.readLine();
			/* Replace ' '+ with ';' for splitting properly */
			strLine = strLine.replaceAll(" +", ";");
			split = strLine.split(";");
			
			/* Create Arc */
			Arc a = new Arc();
			a.setWeight(Math.abs(Integer.parseInt(split[1])));
			//XXX GreatSPN arcs can be -weight *if* it is a broken arc
			a.setSrc(Integer.parseInt(split[2]) - 1);
			a.setSrcLabel('P');
			a.setSrcStr(places.get(a.getSrc()).getLabel());
			a.setDst(t.get_id());
			a.setDstLabel('T');
			a.setDstStr(t.getLabel());
			a.setType(ArcType.NORMAL);
			
			/* Add arc*/
			arcs.add(a);
			
			int nInterPoints = Integer.parseInt(split[3]);
			for(int j = 0; j < nInterPoints; j++)
				strLine = br.readLine(); // Read as much lines as intermediate points
		}
		
		/* Read next line (output arcs) */
		strLine = br.readLine();
		/* Replace ' '+ with ';' for splitting properly */
		strLine = strLine.replaceAll(" +", ";");
		split = strLine.split(";");
		
		int outputArcs = Integer.parseInt(split[1]);
		/* Check output arcs... */
		for(int i = 0; i < outputArcs; i++)
		{
			/* Read new arc */
			strLine = br.readLine();
			/* Replace ' '+ with ';' for splitting properly */
			strLine = strLine.replaceAll(" +", ";");
			split = strLine.split(";");
			
			/* Create Arc */
			Arc a = new Arc();
			a.setWeight(Math.abs(Integer.parseInt(split[1])));
			//XXX GreatSPN arcs can be -weight *if* it is a broken arc
			a.setSrc(t.get_id());
			a.setSrcLabel('T');
			a.setSrcStr(t.getLabel());;
			a.setDst(Integer.parseInt(split[2]) - 1);
			a.setDstLabel('P');
			a.setDstStr(places.get(a.getDst()).getLabel());
			a.setType(ArcType.NORMAL);
			
			/* Add arc*/
			arcs.add(a);
			
			int nInterPoints = Integer.parseInt(split[3]);
			for(int j = 0; j < nInterPoints; j++)
				strLine = br.readLine(); // Read as much lines as intermediate points
		}
		
		/* Read next line (inhibitors arcs) */
		strLine = br.readLine();
		/* Replace ' '+ with ';' for splitting properly */
		strLine = strLine.replaceAll(" +", ";");
		split = strLine.split(";");
		
		/* Check inhibitor arcs */
		int inhibitArcs = Integer.parseInt(split[1]);
		for(int i = 0; i < inhibitArcs; i++)
		{
			/* Read new arc */
			strLine = br.readLine();
			/* Replace ' '+ with ';' for splitting properly */
			strLine = strLine.replaceAll(" +", ";");
			split = strLine.split(";");
			
			/* Create Arc */
			Arc a = new Arc();
			a.setWeight(Math.abs(Integer.parseInt(split[1])));
			//XXX GreatSPN arcs can be -weight *if* it is a broken arc
			a.setSrc(Integer.parseInt(split[2]) - 1);
			a.setSrcStr(places.get(a.getSrc()).getLabel()); // XXX
			a.setSrcLabel('P');
			a.setDst(t.get_id());
			a.setDstLabel('T');
			a.setDstStr(t.getLabel());
			a.setType(ArcType.INHIBITOR);
			
			/* Add arc*/
			arcs.add(a);
			
			int nInterPoints = Integer.parseInt(split[3]);
			for(int j = 0; j < nInterPoints; j++)
				strLine = br.readLine(); // Read as much lines as intermediate points
		}
		/* Add transition */
		return new TransitionArcs(t, arcs);
	}
	
	public int processNewGroup(String strLine)
	{
		strLine = strLine.replaceAll(" +", ";");
		String[] split = strLine.split(";");
		
		// Return priority of the group, directly
		return Integer.parseInt(split[3]);
	}

	public Place processNewPlace(int id, String strLine, boolean changelabel) {
		/* This is a place, create a new node to him */
		Place p = new Place();
		
		strLine = strLine.replaceAll(" +", ";");
		String[] split = strLine.split(";");
		p.set_id(id);
		
		p.setLabel(changelabel ? "p" + id + "_" + split[0] : split[0]);
		p.setInitialMarking(Integer.parseInt(split[1])); 
		p.setY((Double.parseDouble(split[3]))*FACTOR);
		p.setX((Double.parseDouble(split[2]))*FACTOR);
		
		/* Add place */
		return p;
	}
	
	public String createHeaderFile(int nPlaces, int nTransitions)
	{
		StringBuilder sb = new StringBuilder();
		
		//XXX Curiously, GreatSPN limits max places and transitions by the parsing engine (4 leading spaces)
		sb.append("|0|\n\n\n|\n");
		sb.append("f   0" + 
					String.format("%4d", nPlaces) + 
					"   0" + 
					String.format("%4d", nTransitions)  + 
					"   1   0   0\n");
		
		return sb.toString();
	}
	
	public String createGroupDescription()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("G1  0.833333 0.208333 1\n");
		
		return sb.toString();
	}
	
	public String createDefFileContent()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("|256\n%\n|\n");
		
		return sb.toString();
	}
	
	public String createPlace(Place p)
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(	p.getLabel() + "    " + 
					p.getInitialMarking() + " " +
					String.format(Locale.ENGLISH, "%.6f", p.getX()/FACTOR) + " " + String.format(Locale.ENGLISH, "%.6f", p.getY()/FACTOR) + " " +
					String.format(Locale.ENGLISH, "%.6f", p.getX()/FACTOR) + " " + String.format(Locale.ENGLISH, "%.6f", p.getY()/FACTOR) + " 0\n" 
					);
		
		return sb.toString();
	}

	public String createTransition(Transition t, 
									ArrayList<PlaceArc> idxPrePlaces, 
									ArrayList<PlaceArc> idxPostPlaces) {
		StringBuilder sb = new StringBuilder();
		
		sb.append((t.isTimed() ? t.getLabel() : t.getLabel().toLowerCase()) + "  " +  // id
				String.format(Locale.ENGLISH, "%.6e", (t.isTimed() ? t.getRate() : t.getWeight())) + "   " + // rate/weight
				(t.isInfiniteServer() ? "0" : "1") + "   " +// server type
				(t.isTimed() ? "0" : "1") + "   " + // transition kind (XXX only exp and immediate!) 
				idxPrePlaces.size() + " " +// number of input arcs
				"0 " + // rotation coefficient
				String.format(Locale.ENGLISH, "%.6f", t.getX()/FACTOR) + " " + String.format(Locale.ENGLISH, "%.6f", t.getY()/FACTOR) + " " +
				String.format(Locale.ENGLISH, "%.6f", t.getX()/FACTOR) + " " + String.format(Locale.ENGLISH, "%.6f", t.getY()/FACTOR) + " " +
				String.format(Locale.ENGLISH, "%.6f", t.getX()/FACTOR) + " " + String.format(Locale.ENGLISH, "%.6f", t.getY()/FACTOR) + " " +
				"0\n" // levels & col
				);
		// Input arcs
		for(PlaceArc ipa : idxPrePlaces)
			sb.append(createArc(ipa));

		sb.append("   " + idxPostPlaces.size() + "\n"); // number of output arcs
		// Output arcs
		for(PlaceArc opa : idxPostPlaces)
			sb.append(createArc(opa));
		
		sb.append("   0\n"); // number of inhibitor arcs
		// Inhibitor arcs
		//XXX TODO
		return sb.toString();
	}
	
	private String createArc(PlaceArc pa)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("   " + pa.weight + // arc multiplicity
				String.format("%4d", (pa.place.get_id() + 1)) + // idx place which connects to
				// XXX Add one because GreatSPN starts counting at 1 instead of 0
				"   0 0\n"); // arc points & levels col
		
		return sb.toString();
	}
	
	public double readThroughputResults(String tLabel, String netfile)
	{
		ArrayList<TransitionThr> array = readThroughputsResults(tLabel, netfile);
		
		
		if(array.size() == 0){
			  System.out.println("Transition [" + tLabel+"] not found in " + netfile + ". Please check!");
			  return 0;
		}else
			  System.out.println("Throughput of [" + tLabel + "]: " + array.get(0).thrValue);
		
		return array.get(0).thrValue;
	}
	
	private ArrayList<TransitionThr> readThroughputsResults(String labelIDToCheck, String netfile)
	{
		ArrayList<TransitionThr> array = new  ArrayList<TransitionThr>(1);
		FileInputStream fstream = openResultsFile(netfile);
		
		try{
		  // Get the object of DataInputStream
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  
		  String strLine;
		  while((strLine = br.readLine()) != null)
		  {
			  TransitionThr tThr = parseThrFileLine(strLine);
			  if(labelIDToCheck != null)
			  {
				  if(tThr.labelID.equals(labelIDToCheck))
				  {
					  array.add(tThr);
					  break;
				  }
			  }else
				  array.add(tThr);
		  }
			  
		  //Close the input stream
		  in.close();
		  fstream.close();
		}
		catch (Exception e)
		{//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
			
		return array;
	}
	
	public ArrayList<TransitionThr> readThroughputResults(String netfile)
	{
		ArrayList<TransitionThr> array = readThroughputsResults(null, netfile);
		return array;
	}
	
	private TransitionThr parseThrFileLine(String strLine) {
		String[] split = strLine.split(" ");		
		/*for(int i = 0; i < split.length; i++)
			  System.out.print(split[i] + ";");
		System.out.println("");*/
		String labelID = split[0].substring(5, split[0].length());
		double thrValue = Double.parseDouble(split[2]);

		return new TransitionThr(labelID, thrValue);
	}

	private FileInputStream openResultsFile(String netfile)
	{
		if(!netfile.endsWith(".net"))
			System.out.println("WARNING! " + netfile + " should end with the proper extension (.net)");
		String staFilename = netfile.substring(0, netfile.length() - 4) + ".sta";
		
		System.out.println("Reading " + staFilename);
		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(staFilename);
		}
		  catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		return fstream;
	}
	
	public static int launchGreatSPNSolver(String netfile, long timeout)
	{
		// XXX Assumes "newSO" is the GreatSPN solver, installed in the system
		if(!netfile.endsWith(".net"))
			System.out.println("WARNING! " + netfile + " should end with the proper extension (.net)");
		
		int exitValue = -1;
		
		Process process = null;
		try {
			process = new ProcessBuilder("newSO", netfile.substring(0, netfile.length() - 4)).start();
		
			/*InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
	
			while ((line = br.readLine()) != null) {
			  System.out.println(line);
			}*/
			//process.waitFor(timeout, TimeUnit.SECONDS);
			exitValue = process.exitValue();
			if(exitValue == 9) // Unable to terminate/abnormal termination
				exitValue = -2;
		} catch (IOException e) {
			e.printStackTrace();
		/*} catch (InterruptedException e) {
			e.printStackTrace();*/
		}catch(IllegalThreadStateException e)
		{
			// Here falls when timeout expires, now return -2
			exitValue = -2;
			process.destroy();
		}
		
		return exitValue;
	}
	
	public static boolean cleanGreatSPNSolverResults(String netfile)
	{
		// XXX Assumes "RemoveResCommand" is the GreatSPN solver, installed in the system
		if(!netfile.endsWith(".net"))
			System.out.println("WARNING! " + netfile + " should end with the proper extension (.net)");
		
		int exitValue = -1;
		
		Process process;
		try {
			process = new ProcessBuilder("RemoveResCommand", netfile.substring(0, netfile.length() - 4)).start();
			process.waitFor();
			exitValue = process.exitValue();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return (exitValue == 0);
	}

	public static int launchGreatSPNSolver(String filename) {
		return launchGreatSPNSolver(filename, 60*5); // second param is the timeout
	}
	
	public static int launchSimulation(String netfile) 
	{
		return launchSimulation(netfile, 60*10, 99, 1);
	}

	public static int launchSimulation(String netfile, long timeout, int CONFLEVEL, int ACCERROR) {
		if(!netfile.endsWith(".net"))
			System.out.println("WARNING! " + netfile + " should end with the proper extension (.net)");
		
		int exitValue = -1;		
		Process process = null;
		System.out.print("Starting simulation [" + netfile + " (" + CONFLEVEL + "," + ACCERROR + ")]... ");

		try {
			ProcessBuilder pb = new ProcessBuilder("swn_ord_sim", netfile.substring(0, netfile.length() - 4), 
														"-c", String.valueOf(CONFLEVEL), "-a", String.valueOf(ACCERROR));
			
			// Redirect output to NULL 
			// XXX Not Windows portable! However, GreatSPN is not supposed to run on Windows. Touché :)
			//pb.redirectOutput(ProcessBuilder.Redirect.to(new File("/dev/null")));//.INHERIT); //XXX
			pb.redirectErrorStream(true);
			process = pb.start();
		
			//process.waitFor(timeout, TimeUnit.SECONDS); //XXX
			exitValue = process.exitValue();
		} catch (IOException e) {
			e.printStackTrace();
		/*} catch (InterruptedException e) { //XXX
			e.printStackTrace(); */
		} catch (IllegalThreadStateException e)
		{
			// timeout expired :(
			exitValue = -2;
			process.destroy(); // Kill the process
		}
		
		System.out.println("done.");
		return exitValue;
	}

	public String createStatFileContent(ArrayList<Transition> transitions) {
		StringBuilder sb = new StringBuilder();
		
		for(Transition t : transitions)
			sb.append(t.getLabel() + "\n");
		
		return sb.toString();		
	}
}
