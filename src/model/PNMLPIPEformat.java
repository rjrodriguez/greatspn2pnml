/**
 * Filename: PNMLformat.java
 * Date: February, 2012 -- first release
 * Class for defining how PNML nodes are. 
 * TODO Adapt to work directly with PNML framework
 *   
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */

package model;

import java.util.Locale;

public class PNMLPIPEformat extends PNMLformat{
	public String createFileHeader()
	{
		StringBuilder s = new StringBuilder();
		s.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		s.append("<pnml>\n");
		s.append("<net id=\"Net-One\" type=\"P/T net\">\n");
		s.append("<token id=\"Default\" enabled=\"true\" red=\"0\" green=\"0\" blue=\"0\"/>\n");
		
		return s.toString();
	}

	public String createFileTail()
	{
		return "</net>\n</pnml>";
	}

	
	public String createPlaceNode(Place p)
	{
		StringBuilder s = new StringBuilder();
		
		/* Init place description */
		//s.append("\t<place id=\"P" + p.get_id() + "\">\n");
		s.append("\t<place id=\"" + p.getLabel() + "\">\n");
		/* Set graphics coordinates */
		s.append("\t\t<graphics>\n");
		s.append("\t\t\t<position x=\"" + p.getX() + "\" y=\"" + p.getY() +"\" />\n");
		s.append("\t\t</graphics>\n");
		
		/* Set place name */
		s.append("\t\t<name>\n");
		s.append("\t\t\t<value>" + p.getLabel() +"</value>\n");
		s.append("\t\t\t<graphics>\n");
		s.append("\t\t\t\t<offset x=\"0\" y=\"0\" />\n");
		s.append("\t\t\t</graphics>\n");
		s.append("\t\t</name>\n");
		
		/* Set initial marking */
		s.append("\t\t<initialMarking>\n");
		s.append("\t\t\t<value>Default," + p.getInitialMarking() + "</value>\n");
		s.append("\t\t\t<graphics>\n");
		s.append("\t\t\t\t<offset x=\"0\" y=\"0\" />\n");
		s.append("\t\t\t</graphics>\n");
		s.append("\t\t</initialMarking>\n");

		/* Set capacity (no capacity, by default) */
		s.append("\t\t<capacity>\n");
		s.append("\t\t\t<value>0</value>\n");
		s.append("\t\t</capacity>\n");

		/* End place description */
		s.append("\t</place>\n");
		
		return s.toString();
	}

	public String createTransitionNode(Transition t)
	{
		StringBuilder s = new StringBuilder();
		/* Init transition description */
		//s.append("\t<transition id=\"T" + t.get_id() + "\">\n");
		s.append("\t<transition id=\"" + t.getLabel() + "\">\n");

		/* Set graphics coordinates */
		s.append("\t\t<graphics>\n");
		s.append("\t\t\t<position x=\"" + t.getX() + "\" y=\"" + t.getY() + "\" />\n");
		s.append("\t\t</graphics>\n");
		
		/* Set transition name */
		s.append("\t\t<name>\n");
		s.append("\t\t\t<value>" + t.getLabel() + "</value>\n");
		s.append("\t\t\t<graphics>\n");
		s.append("\t\t\t\t<offset x=\"0\" y=\"0\" />\n");
		s.append("\t\t\t</graphics>\n");
		s.append("\t\t</name>\n");
		
		/* Set orientation */
		s.append("\t\t<orientation>\n");
		s.append("\t\t\t<value>" + t.getOrientation() +"</value>\n");
		s.append("\t\t</orientation>\n");

		/* Set rate */ 
		// XXX Rate contains weight, in case of immediate transitions
		s.append("\t\t<rate>\n");
		s.append(String.format(Locale.ENGLISH, "\t\t\t<value>%f</value>\n", t.getRate()));
		s.append("\t\t</rate>\n");

		/* Set immediate/timed */
		s.append("\t\t<timed>\n");
		s.append("\t\t\t<value>" + t.isTimed() + "</value>\n");
		s.append("\t\t</timed>\n");

		/* Set firing semantics */
		s.append("\t\t<infiniteServer>\n");
		s.append("\t\t\t<value>" + t.isInfiniteServer() + "</value>\n");
		s.append("\t\t</infiniteServer>\n");

		/* Set priority */
		s.append("\t\t<priority>\n");
		s.append("\t\t\t<value>" + t.getPriority() + "</value>\n");
		s.append("\t\t</priority>\n");

		/* End transition description */
		s.append("\t</transition>\n");

		return s.toString();
	}
	
	public String createArcNode(Arc a)
			//, ArrayList<Place> places, ArrayList<Transition> transitions)
	{
		StringBuilder s = new StringBuilder();
		
		/*ArrayList<?> srcAux = null, 
							dstAux = null;
		if(a.getSrcLabel() == 'P')
		{
			srcAux = places;
			dstAux = transitions;
		}else
		{
			srcAux = transitions;
			dstAux = places;
		}
		
		if(a.getSrcStr() == null)
		{
			a.setSrcStr(((PNObject)srcAux.get(a.getSrc())).getLabel());
		}
		if (a.getDstStr() == null)
		{
			a.setDstStr(((PNObject)dstAux.get(a.getDst())).getLabel());
		}*/
		
		/* Init arc node */
		s.append( "\t<arc id=\"" +
					a.getSrcStr() + " to " + 
					a.getDstStr() + "\" source=\"" + 
					a.getSrcStr() + "\" target=\"" + 
					a.getDstStr() + "\">\n");
					/*((PNObject)srcAux.get(a.getSrc())).getLabel() + " to " + 
					((PNObject)dstAux.get(a.getDst())).getLabel() + "\" source=\"" + 
					((PNObject)srcAux.get(a.getSrc())).getLabel() + "\" target=\"" + 
					((PNObject)dstAux.get(a.getDst())).getLabel() + "\">\n");*/
		s.append( "\t\t<graphics/>\n");
		/* Set weight inscription */
		s.append( "\t\t<inscription>\n");
		s.append( "\t\t\t<value>Default," + a.getWeight() + "</value>\n");
		s.append( "\t\t</inscription>\n");
		
		/* Set tagged value */
		s.append( "\t\t<tagged>\n");
		s.append( "\t\t\t<value>false</value>\n"); //XXX False tagged by default
		s.append( "\t\t</tagged>\n");
		
		/* XXX No arc paths */
		// Not needed?
		
		/* Set arc type */
		String strType = "";
		if(a.getType() == ArcType.NORMAL)
			strType = "normal";
		else
			strType = "inhibitor";
		s.append( "\t\t<type value=\"" + strType + "\"/>\n");
		
		/* End arc node */
		s.append( "\t</arc>\n");

		
		return s.toString();
	}
}
