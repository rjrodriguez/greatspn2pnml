/**
 * Filename: Transition.java
 * Date: February, 2012 -- first release
 * Defines a Transition at PN level
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */
package model;

public class Transition extends PNObject
{
	private double rate;
	private boolean isTimed;
	private boolean infiniteServer;
	private int priority;
	private int orientation;
	private double weight;
	
	/* Getters and setters */
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public boolean isTimed() {
		return isTimed;
	}
	public void setTimed(boolean isTimed) {
		this.isTimed = isTimed;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public boolean isInfiniteServer() {
		return infiniteServer;
	}
	public void setInfiniteServer(boolean infiniteServer) {
		this.infiniteServer = infiniteServer;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getOrientation() {
		return orientation;
	}
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}
	public double getWeight() {
		return this.weight;
	}
}