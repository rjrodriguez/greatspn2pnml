/**
 * Filename: greatspn2PNML.java
 * Date: February, 2012 -- first release
 * Main class. It needs two arguments when invoking: the .net file (from the GreatSPN) and the file to write out the 
 * PNML format of the PN description
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of greatspn2PNML.
 *
 * greatspn2PNML is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * greatspn2PNML is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with greatspn2PNML.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import model.Arc;
import model.GreatSPNformat;
import model.GreatSPNformat.TransitionArcs;
import model.PNMLPIPEformat;
import model.PNMLTimeNETformat;
import model.PNMLType;
import model.PNMLformat;
import model.Place;
import model.Transition;


public class greatspn2PNML {
	ArrayList<Place> places;
	ArrayList<Transition> transitions;
	ArrayList<Arc> arcs;
	int[] groups;
	
	private GreatSPNformat gspnfile ;
	
	public static void printUsage()
	{
		System.out.println(	"Insufficient parameters.\n" +
							"Usage: greatspn2PNML <greatspn_file> <output_PNML_file> [-t PNMLformat] [--change-labels]\n"
							+ "Supported PNMLformat values: pipe, timenet\n"
							+ "Example: greatspn2PNML myNet.net myNet.xml -t pipe");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/* It receives as parameter the source and the destiny file */
		int i = 2;
		String arg, pnmlType = "";
		boolean typeflag = false, changelabels = false;
		
		greatspn2PNML g2PNML = new greatspn2PNML();
		
		while (i < args.length && args[i].startsWith("-")) {
            arg = args[i++];
            
            if(arg.equals("-t"))
            {
            	if (i < args.length){
                    pnmlType = args[i++];
                    if(pnmlType.matches(PNMLType.PIPE) || pnmlType.matches(PNMLType.TIMENET))
                    	typeflag = true;
            	}else{
                    System.err.println("-t requires to specify the PNML type (pipe, timenet)");
                    printUsage();
                    System.exit(-1);
                }
            }else if(arg.equals("--change-labels"))
            {
            	changelabels = true;
            }
		}
		if (!typeflag){
			printUsage();
			System.exit(-1);
		}
		
		g2PNML.parseFile(args[0], args[1], pnmlType, changelabels);
		
	}

	private void parseFile(String srcFile, String dstFile, String pnmlType, boolean changelabels) {
		try{
			  // Open the file that is the first 
			  // command line parameter
			  FileInputStream fstream = new FileInputStream(srcFile);
			  // Get the object of DataInputStream
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  
			  //Read File Line By Line
			  parseGreatSPNFile(br, changelabels);
			  System.out.println("Converting to PNML (|P|=" + places.size() + ", |T|=" + transitions.size() +")...");
			  
			  //Close the input stream
			  in.close();
			  fstream.close();
			  
			  /* OK, now convert to PNML! */
			  saveToPNML(dstFile, pnmlType);
			  System.out.println("Conversion done. Exiting...");
		}
		catch (Exception e)
		{//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	private void saveToPNML(String filename, String pnmlType) throws IOException {
		FileOutputStream fos = new FileOutputStream(filename); 
		OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8");
		
		PNMLformat pnml= null;
		// Create appropiate PNML format 
		if(pnmlType.matches(PNMLType.PIPE))
			pnml = new PNMLPIPEformat();
		else if (pnmlType.matches(PNMLType.TIMENET))
			pnml = new PNMLTimeNETformat();
		else
		{
			printUsage();
			System.exit(-1);
		}
		
		out.write(pnml.createFileHeader());
		
		/* Iterate on places */
		for(Place p : places)
		{
			out.write(pnml.createPlaceNode(p));
		}
		
		/* Iterate on transitions */
		for(Transition t : transitions)
		{
			out.write(pnml.createTransitionNode(t));
		}
		
		/* Iterate on arcs */
		for(Arc a : arcs)
		{
			out.write(pnml.createArcNode(a));
		}
		
		out.write(pnml.createFileTail());
		/* Close everything */
		out.flush();
		out.close();
		fos.close();
	}

	private void parseGreatSPNFile(BufferedReader br, boolean changelabels) throws IOException {
		String strLine;
		
		places = new ArrayList<Place>(1);
		transitions = new ArrayList<Transition>(1);
		arcs = new ArrayList<Arc>(1);
		
		gspnfile = new GreatSPNformat();

		strLine = br.readLine();
		while (strLine != null && !strLine.startsWith("f"))
		{
			strLine = br.readLine(); // Read next one
		}	
		// Avoid errors
		if(strLine == null)
		{
			return; 
		}
			
		/* Ok, collect data: no. places and no. transitions */
		/* Replace ' '+ with ';' for splitting properly */
		strLine = strLine.replaceAll(" +", ";");
		String[] split = strLine.split(";");
		
		int 	nMarkingParameters = Integer.parseInt(split[1]),
				nPlaces = Integer.parseInt(split[2]),
				nRateParameters = Integer.parseInt(split[3]),
				nTransitions = Integer.parseInt(split[4]),
				nGroups = Integer.parseInt(split[5]);
		
		// XXX Respect the order of parsing, otherwise errors will arise
		readMarkingParameters(nMarkingParameters, br);
		readPlaces(nPlaces, br, changelabels);
		readRateParameters(nRateParameters, br);
		readGroups(nGroups, br);
		readTransitions(nTransitions, br, changelabels);
	}

	private void readTransitions(int nTransitions, BufferedReader br, boolean changelabels) throws IOException {
		String strLine;
		for(int i = 0; i < nTransitions && (strLine = br.readLine()) != null; i++)
		{
			TransitionArcs ta = gspnfile.processNewTransition(places, transitions.size(), groups, strLine, br, changelabels);
			
			// XXX needed since greatSPN allows repetitive labels
			// I detected some weird issues when using same case-sensitive labels.
			// XML file is correct, but GUI does not connect well the arcs
			for(int j = 0; j < transitions.size(); j++)
			{
				Transition tt = transitions.get(j);
				if(tt.getLabel().equalsIgnoreCase(ta.t.getLabel()))
				{
					ta.t.setLabel("t" + ta.t.getLabel()); // re-label
					j = 0; // Starts iteration again
				}
			}	
			
			transitions.add(ta.t);
			arcs.addAll(ta.arcs);
		}
	}


	private void readGroups(int nGroups, BufferedReader br) throws IOException {
		@SuppressWarnings("unused")
		String strLine;
		groups = new int[nGroups];
		
		for(int i = 0; i < nGroups && (strLine = br.readLine()) != null; i++)
		{
			groups[i] = gspnfile.processNewGroup(strLine);
		}
	}

	private void readRateParameters(int nRateParameters, BufferedReader br) throws IOException {
		@SuppressWarnings("unused")
		String strLine;
		for(int i = 0; i < nRateParameters && (strLine = br.readLine()) != null; i++)
		{
			// Do nothing in this case
		}
	}

	private void readPlaces(int nPlaces, BufferedReader br, boolean changelabels) throws IOException {
		String strLine;
		for(int i = 0; i < nPlaces && (strLine = br.readLine()) != null; i++)
		{
			// XXX needed since greatSPN allows repetitive labels
		    // I detected some weird issues when using same case-sensitive labels.
			// XML file is correct, but GUI does not connect well the arcs
			Place p = gspnfile.processNewPlace(places.size(), strLine, changelabels);
			for(int j = 0; j < places.size(); j++)
			{
				Place pp = places.get(j);
				if(pp.getLabel().equalsIgnoreCase(p.getLabel()))
				{
					p.setLabel("p" + p.getLabel()); // re-label
					j = 0; // Starts iteration again
				}
			}		
			
			places.add(p);
			
			
		}
	}

	private void readMarkingParameters(int nMarkingParameters, BufferedReader br) throws IOException {
		@SuppressWarnings("unused")
		String strLine;
		for(int i = 0; i < nMarkingParameters && (strLine = br.readLine()) != null; i++)
		{
			// Do nothing in this case
		}
	}

	
}
